FROM node:8.16.0-alpine
MAINTAINER Arseniy Popov <parsenicum@gmail.com>


WORKDIR /

RUN npm install -g nodemon

COPY package*.json ./

RUN npm install

COPY src/* ./

EXPOSE 3000
CMD [ "npm", "run", "dev" ]
