import TipsApi from '../../api/TipsApi'

const tipsApi = new TipsApi()

describe('TipsAPI', () => {
  it('should save a new tip', async done => {
    const tipData = "hello world"

    const data = await tipsApi.push(tipData)

    expect(data).toEqual({
      success: 1
    })

    done()

  })

  it('should get a tip', async done => {
    const result = await tipsApi.get("hel")

    expect(result).toHaveLength(1)
    done()

  })

  it('should correct sort of tips', async done => {
    const data = [
      '1a',
      '1a',
      '1a',
      '11',
      '11',
      '12',
      '13',
      '14',
      '15',
      '1a',
      '1a',
      '1b'
    ]

    for ( const query of data ) {
      await tipsApi.push(query)
    }

    const result = await tipsApi.get('1')

    expect(result).toStrictEqual([
      '1a',
      '11',
      '12',
      '13'
    ])

    done()

  })

  it('should contains tips only by first symbols', async done => {

    const data = [
      'Mr, hello world',
      'world hello'
    ]

    for ( const query of data ) {
      await tipsApi.push(query)
    }

    const result = await tipsApi.get("ell")
    expect(result).toHaveLength(0)

    done()
  })

  it('should relevance users tips', async done => {
    done()
  })

})
