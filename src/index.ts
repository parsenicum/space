import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'

import TipsApi from './api/TipsApi'

const app = express()
const tipsApi = new TipsApi()

app.use(bodyParser.json())
app.use(cors())

app.post('/', ( { body } : express.Request, res: express.Response) => {
  res.json(
    body.query ? tipsApi.push(body.query, body.userId) : { error: "no query in body" }
  )
})

app.get('/ping', (_,res) => res.send('pong'))

app.get('/top', async ( _ , res: express.Response) => {
  try {
    const result = await tipsApi.top()

    res.json({
      result
    })

  } catch (e) {
    res.json({error: "server error"})

  }
})

app.get('/', ({ query }: express.Request, res: express.Response) => {
  if ( query.query ) {
    tipsApi.get(query.query, query.userId )
    .then( result => res.json({result}))
    .catch( err => res.json({
        error: "server error"
    }))
  } else {
    res.json({
        error: "no query in query"
    })
  }
})

app.all('*', (_, res) => res.send('welcome to tips server'))

app.listen(3500, () => {
  console.log('server started on port 3500')
})
