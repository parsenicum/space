let tips: Map<string, number> = new Map()

export default class TipsApi {

  async push(data: string, userId?: string) {

    if ( tips.has(data) ) {
      tips.set(data, tips.get(data) + 1)

      return {
        success: 0
      }

    } else {
      tips.set(data, 1)

      return {
        success: 1
      }
    }

  }

  async get(query: string, userId?: string) {

    return Array.from(tips).filter(
      ( [ item, q ] ) =>
        item.search( new RegExp(query, "gi") ) === 0

    ).sort(
      ([ _, a ], [ __, b ]) =>
        a > b ? -1 : 1

    ).slice( 0, 4 ).map( ([tip, _ ]) => tip )

  }

  async top( count = 10 ){
    return Array.from(tips).sort(
      ([ _, a ], [ __, b ]) =>
        a > b ? -1 : 1

    ).slice( 0, count ).map( ([tip, _ ]) => tip )
  }

  async all(){
    return Array.from(tips)
  }

}
