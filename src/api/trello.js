var Trello = require("trello"); // здесь чекать методы https://github.com/norberteder/trello/blob/master/main.js
const Promise = require('bluebird');
const axios = require('axios');
const moment = require('moment');
const fs = Promise.promisifyAll(require("fs"), {suffix: "Async"});
const scraper = require('google-search-scraper');
const GoogleSearchScraper = require('phantom-google-search-scraper');



const creds = {
  key: "528f11713b77d2523cc560b4d24403dd",
  token: "f4405f3b74e79d0c6150ac2f5293613fce0fa59465ffe7d6f6bb596b8bc25400"
}
var trello = new Trello(creds.key, creds.token);

// const t = axios.instance({
// })

// .addCard = function (name, description, listId) - бот генерит даты и время постинга - имя карточки
// бот выкатывает на биржу тз https://www.etxt.ru/api/
// модерирую
// бот забирает контент
// бот выкавает в соц сети

const generateFeed = async (boardId , mounthName , days ) => {
  const list = await trello.addListToBoard( boardId , mounthName || 'январь' );
  console.log(list);

  (new Array(days || 31).fill(0)).map( (e,i)=> i + 1).reduce( async (promiseChain, item) => {
    await promiseChain;
    console.log("now "+item);
    await trello.addCardWithExtraParams(item , {
      labels: [labels[1]]
    }, list.id);
    return new Promise( resolve => setTimeout( resolve , 250 ) )
  } , Promise.resolve()).then(() => console.log('done'));
}

const main = async ()=> {
  // получить доску трелло
  // let boards = await trello.getBoards('me');
  // let board = boards.find( e => e.name === 'каналы')
  const mediaBoardId = '5c1d4a1eaf883c6a26b14b96';

  //let mediaBoardId = boards[0].id;
  // fs.writeFileSync('./media_board.json', JSON.stringify(boards[0], null, '\t'), 'utf8' );

  // взять id доски
  // let mediaBoardId = JSON.parse(await fs.readFileAsync('./media_board.json', 'utf8')).id;
  // взять текущие лэйблы из файла
  // let labels = JSON.parse(await fs.readFileAsync('./labels.json', 'utf8'));
  // console.log(labels[0]);
  let labelId = '5c1d4a1ec8388fca221940b8'
  // let labels = await trello.getLabelsForBoard(mediaBoardId);
  // console.log(labels); // проставлять лейблы когда составляется медиа план:
  //fs.writeFileSync('./labels.json', JSON.stringify(labels, null, '\t'), 'utf8' );


  // let list = (await trello.getListsOnBoard(mediaBoardId)).find( e => e.name === 'todo (все)');

  // scraper.search({
  //   query: 'rakuten',
  //   limit: 1
  // }, function(err, url, meta) {
  //   // This is called for each result
  //   if(err) throw err;
  //   console.log(url);
  //   // console.log(meta.title);
  //   // console.log(meta.meta);
  //   // console.log(meta.desc)
  // });

  const all = await fs.readFileAsync('./out3.csv' , 'utf8');

  const r = all.split('\n').map(e=> {
    let [name,reg,url] = e.split(';');
    url = url && url.match(/https:\/\/.+\//);
    if(url && url.length) {
      return [ name, url[0] ];
    } else{
      return null;
    }
  }).filter(Boolean).reduce(async (promiseChain, item) => {
    await promiseChain;

    let listId = '5c1d4a2ed813c54048816e85';

    const r = await trello.addCardWithExtraParams(item[0], {
      desc: item[1],
      pos: 'top',
      idLabels: labelId
    }, listId);

  }, Promise.resolve() ).then( () => {
    console.log("done!");
  })



  // all.split('\n').slice(10).filter(o=> o.split(';')[0] && o.split(';')[1] && !/Pages in this section/.test(o.split(';')[1]) )
  // .reduce(async (promiseChain, item) => {
  //     await promiseChain;
  //     const [a,b] = item.split(';');
  //     let c = 'url';
  //     try {
  //       const result = await GoogleSearchScraper.search({
  //         query : a + ' marketplace', // Query for google engine
  //         limit: 1, // Limit number of results
  //         keepPages: false, // Populate results.pages with rendered HTML content.
  //         //solver: GoogleScraper.commandLineSolver, // Optional solver for resolving captcha (see commandLineSolver.js)
  //         userAgent: 'GoogleSearchScraper1.0',
  //         headers: { // Default http headers for PhantomJS
  //           'Accept-Language': 'ru-RU,en,*'
  //         },
  //         phantomOptions: [ // Command line options use for PhantomJS
  //           '--ignore-ssl-errors=yes'
  //         ]
  //       });
  //
  //       c = result.urls[0];
  //     } catch (e) {
  //
  //     }
  //     final.push(a + ';' + b +';' + c);
  //     // await fs.writeFileAsync('./out3.csv', final.join('\n'), 'utf8');
  // }, Promise.resolve()).then( e => {
  //   console.log(1);
  // });

  // let listId = '5c1d4a2ed813c54048816e85';
  //
  // const r = await trello.addCardWithExtraParams('name', {
  //   desc: '',
  //   pos: 'top',
  //   idLabels: labelId
  // }, listId);
  //
  // console.log(r);
  // let cards = await trello.getCardsOnListWithExtraParams(list.id, { attachments: 'true'});//await trello.getCardsOnList(list.id);
  // cards = normalize(cards);
  //
  // cards = cards.filter( card => {
  //   let r = false;
  //   if(card.labels && card.labels.length){
  //     card.labels.forEach( e=> r = r || e.id === '5c011628e093257939c99a2d' )
  //   }
  //   return r;
  // })
  // console.log(cards.map( e => e.name ));
  // await trello.updateCardDescription(cards[0].id, "hello world");
  // console.log(cards);
  //const {data} = await axios.get(`https://api.trello.com/1/members/me/boards?key=${creds.key}&token=${creds.token}`)
}

main()

function normalize(arr){
  return arr.map( e=> {
    const { id , name , desc , attachments , labels } = e;
    const result = { id , name , desc , attachments , labels };
    const lbls = [];
    const attach = [];
    if(attachments.length){
      attachments.forEach( a=> attach.push(a.url));
      result.attachments = attach;
    }
    if(labels.length){
      labels.forEach( l=> { const { id, name, color} = l; lbls.push({ id, name, color}) });
      result.labels = lbls;
    }
    return result;
  });
}

// структура карточки
// описание карточки - готовая работа: она идет в ленту новостей
// addCommentToCard = function (cardId, comment) - в коментариях правки , первый коментраций - тз
// .addAttachmentToCard = function (cardId, url) - прикладываются картинки (они идут вместе с новостью)

// как будет работать бот:
// .getCard(boardId, cardId) - получает карточку (вытаскивает имя, описание, приложения)
// постит в соц сеть

const exportVk = async _api => {
  let mediaBoardId = JSON.parse(await fs.readFileAsync('./media_board.json', 'utf8')).id;
  let list = (await trello.getListsOnBoard(mediaBoardId)).filter( e => e.name === 'декабрь')[0];
  let cards = await trello.getCardsOnListWithExtraParams(list.id, { attachments: 'true'});//await trello.getCardsOnList(list.id);
  normalize(cards).filter( card => {
    let r = false;
    if(card.labels && card.labels.length){
      card.labels.forEach( e=> r = r || e.id === '5c011628e093257939c99a2d' )
    }
    return r;
  }).reduce(async (promiseChain, item) => {
      await promiseChain;
      const card = item;
      const photo = card.attachments && card.attachments.length ? { photos: card.attachments}:{};
      try {
          const { data } = await axios.post( _api , { ...photo , ...{
            message: card.desc ,
            inc_days: moment(`2018-12-${tm.d(card.name)}T${tm.h()}:${tm.m()}:${tm.m()}.000Z`).unix(),
            guid: card.id + new Date().getTime()
          }});
          console.log(data);
          if(data.response && data.response.post_id){
            await trello.deleteLabelFromCard(card.id, "5c011628e093257939c99a2d");
            await trello.addLabelToCard(card.id, "5c011fa7fa499b68a8dc8d09");
          }
      } catch (e){
          console.log("FAIL WITH "+card.id);
          console.log(e);
      }
      return new Promise( (resolve) => {
        asyncFunction(item, resolve);
      });
  }, Promise.resolve()).then(() => console.log('done'));

  function asyncFunction (item, cb) {
    setTimeout(() => {
      //console.log('done with', item);
      cb();
    }, 500);
  }

  // .forEach( async (card, index) => {
  //   //console.log(card.desc + card.attachments);
  //   const photo = card.attachments && card.attachments.length ? { photos: card.attachments}:{};
  //   try {
  //     const { data } = await axios.post(api , { ...photo , ...{
  //       message: card.desc ,
  //       inc_days: moment(`2018-12-${tm.d(card.name)}T${tm.h()}:${tm.m()}:${tm.m()}.000Z`).unix(),
  //       guid: card.id
  //     }});
  //     console.log(data);
  //   } catch (e){
  //     console.log(e.data);
  //   }
  // });
}


var tm = {
  d: n => {
    return parseInt(n) > 10 ? n : '0'+n;
  },
  m: () => {
    let t = Math.round(Math.random() * 30) + 10;
    return t > 10 ? t : '0'+t;
  },
  h: () => {
    let t = Math.round(Math.random() * 12) + 6;
    return t > 10 ? t : '0'+t;
  }
}

// exportVk("http://localhost:3000/news"); // need run export vk
